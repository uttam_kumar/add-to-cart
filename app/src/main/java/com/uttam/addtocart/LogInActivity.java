package com.uttam.addtocart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LogInActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText userEditText,passEditText;
    private Button signInBtn,signUpBtn;
    private String userName,password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        initialViews();
        clickListener();
    }


    private void initialViews() {
        userEditText=findViewById(R.id.usernameId);
        passEditText=findViewById(R.id.passwordId);
        signInBtn=findViewById(R.id.signInBtnId);
        signUpBtn=findViewById(R.id.signUpBtnId);
    }

    private void clickListener() {

        signInBtn.setOnClickListener(this);
        signUpBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signInBtnId:
                if(notNullCheckEditField()){
                    Toast.makeText(this,"logged in",Toast.LENGTH_SHORT).show();
                    Log.d("TAG", "onClick11: "+password);
                }else{
                    Toast.makeText(this,"field can not be null",Toast.LENGTH_SHORT).show();
                }
                break;

            case R.id.signUpBtnId:
                Intent intent=new Intent(LogInActivity.this,SignUpActivity.class);
                startActivity(intent);
                break;
        }
    }

    private boolean notNullCheckEditField() {
        boolean result=true;

        userName=userEditText.getText().toString();
        password=passEditText.getText().toString();

        if(userName.trim().equals("")) {
            result=false;
        }
        if(password.trim().equals("")){
            result=false;
        }
        return result;
    }
}
