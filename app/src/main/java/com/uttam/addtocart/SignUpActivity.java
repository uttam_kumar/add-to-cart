package com.uttam.addtocart;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener  {

    private Button signUpBtn;
    private EditText nameEdit,emailEdit,phone1Edit,phone2Edit,addressEdit,pAddressEdit,passwordEdit,confirmPasswordEdit;
    private String name,email,address1,address2,phone1,phone2,pass,rePass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);

        initialViews();
        clickListener();
    }

    private void initialViews() {
        //edittext
        nameEdit=findViewById(R.id.nameEditId);
        emailEdit=findViewById(R.id.emailEditId);
        phone1Edit=findViewById(R.id.phone1EditId);
        phone2Edit=findViewById(R.id.phone2EditId);
        addressEdit=findViewById(R.id.addressEditId);
        pAddressEdit=findViewById(R.id.pAddressEditId);
        passwordEdit=findViewById(R.id.passwordEditId);
        confirmPasswordEdit=findViewById(R.id.rePasswordEditId);

        //button
        signUpBtn=findViewById(R.id.signUpBtnId);
    }

    private void clickListener() {
        signUpBtn.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signUpBtnId:
                //notNullCheckEditField();
                if(notNullCheckEditField()) {
                    //Intent intent = new Intent(SignUpActivity.this, SignUpActivity.class);
                    //startActivity(intent);
                    if(pass.equals(rePass)) {
                        Toast.makeText(this, "ok", Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(this, "password mismatch", Toast.LENGTH_SHORT).show();
                    }
                }else{
                    Toast.makeText(this,"field can not be null",Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private boolean notNullCheckEditField() {
        boolean result=true;

        name=nameEdit.getText().toString();
        email=emailEdit.getText().toString();
        phone1=phone1Edit.getText().toString();
        phone2=phone2Edit.getText().toString();
        address1=addressEdit.getText().toString();
        address2=pAddressEdit.getText().toString();
        pass=passwordEdit.getText().toString();
        rePass=confirmPasswordEdit.getText().toString();

        if(name.trim().equals("")) {
            result=false;
        }
        if(email.trim().equals("")){
            result=false;
        }
        if(address1.trim().equals("")) {
            result=false;
        }
        if(address2.trim().equals("")){
            result=false;
        }
        if(phone1.trim().equals("")) {
            result=false;
        }
        if(pass.equals("")){
            result=false;
        }
        if(rePass.equals("")){
            result=false;
        }

        return result;
    }
}
