package com.uttam.addtocart;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

public class ObjectMaking extends AppCompatActivity {

    EditText edt1,edt2,edt3,edt4;
    Button btn1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_object_making);

        edt1=findViewById(R.id.editTextId_1);
        edt2=findViewById(R.id.editTextId_2);
        edt3=findViewById(R.id.editTextId_3);
        edt4=findViewById(R.id.editTextId_4);

        btn1=findViewById(R.id.makeObjectBtnId);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject msg = new JSONObject();
                try {
                    JSONObject msg2 = new JSONObject();
                    msg2.put("name", edt2.getText().toString());
                    msg2.put("phone 1", edt3.getText().toString());
                    msg2.put("pone 2", edt4.getText().toString());

                    msg.put("id", edt1.getText().toString());
                    msg.put("value",msg2);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("TAG", "onClick: "+msg.toString());

            }
        });
    }
}
