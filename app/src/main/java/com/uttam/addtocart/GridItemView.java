package com.uttam.addtocart;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class GridItemView extends FrameLayout {

//    private TextView textView;
//
//    public GridItemView(Context context) {
//        super(context);
//        LayoutInflater.from(context).inflate(R.layout.grid_item, this);
//        textView = (TextView) getRootView().findViewById(R.id.textId);
//    }
//
//    public void display(String text, boolean isSelected) {
//        textView.setText(text);
//        display(isSelected);
//    }
//
//    public void display(boolean isSelected) {
//        textView.setBackgroundResource(isSelected ? R.drawable.green_square : R.drawable.gray_square);
//    }

    private ImageView imageView;
    private Bitmap bitmap;

    public GridItemView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.grid_item, this);
        imageView = (ImageView) getRootView().findViewById(R.id.imageId);
    }

    public void display(String text, boolean isSelected) {

        //imageView.setImageBitmap(getBitmapFromURL(text));
        //display(isSelected);
        String displayUrl=text;
        try {
            InputStream str=new URL(displayUrl).openStream();
            bitmap=BitmapFactory.decodeStream(str);
            imageView.setImageBitmap(bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void display(boolean isSelected) {
        imageView.setBackgroundResource(isSelected ? R.drawable.green_square : R.drawable.gray_square);
    }



    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            Bitmap myBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//            connection.setDoInput(true);
//            connection.connect();
//            InputStream input = connection.getInputStream();
//            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            // Log exception
            return null;
        }
    }


}