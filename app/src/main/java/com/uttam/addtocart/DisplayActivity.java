package com.uttam.addtocart;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DisplayActivity extends AppCompatActivity {

    private GridView gridView;
    private String displayImageUrl;
    private ProgressDialog progressDialog;
    private String[] desc;
    private String[] imageUrl;
    private  String TAG="TAG";
    private CustomAdapter customAdapter;
    private ArrayList<String> selectedStrings;
    private Button checkSelectedBtn;
    private boolean isLongClicked=false;
    private GridItemView gridItemView;
    private View vv;
    int ii=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);

        gridView = (GridView) findViewById(R.id.displayDataInage);
        checkSelectedBtn=findViewById(R.id.checkBtnId);
        gridItemView=new GridItemView(this);

        progressDialog=new ProgressDialog(this);
        //calling the method to display the heroes

        progressDialog.setMessage("Retrieving...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        displayImageFromServer();
    }

    private void displayImageFromServer() {
        displayImageUrl= "https://kaishacse.000webhostapp.com/addtocart/Api.php?apicall=getallimages";

        StringRequest stringRequest=new StringRequest(Request.Method.POST,displayImageUrl, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                //Log.d("TAG", "onResponse: "+response);
                try {
                    JSONObject jsonObject=new JSONObject(response);
                    //Log.d("TAG", "onResponse: jsonObject: "+jsonObject);
                    boolean error=jsonObject.getBoolean("error");
                    //Log.d(TAG, "onResponse: error: "+error);
                    if(!error){
                        JSONArray imageArray=jsonObject.getJSONArray("images");
                        desc = new String[imageArray.length()];
                        imageUrl = new String[imageArray.length()];

                        //Log.d(TAG, "onResponse: "+imageArray);
                        for(int i=0;i<imageArray.length();i++){
                            JSONObject imageObject=imageArray.getJSONObject(i);
                            String des=imageObject.getString("desc");
                            desc[i]=des;
                            String img=imageObject.getString("url");
                            imageUrl[i]=img;
                            Log.d(TAG, "onResponse: "+des);
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //data add to grid view
                addToGridView();
                progressDialog.dismiss();
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("TAG", "onResponse: "+error);
                progressDialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams(){
                Map<String,String> prams=new HashMap<>();

                return prams;
            }
        };

        RequestQueue requestQueue=Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onBackPressed() {
        if(isLongClicked){
            isLongClicked=false;
            addToGridView();
        }else{
            super.onBackPressed();
        }
    }

    private void addToGridView(){
        //customAdapter = new CustomAdapter(desc,DisplayActivity.this);
        customAdapter = new CustomAdapter(imageUrl,DisplayActivity.this);
        gridView.setAdapter(customAdapter);
        selectedStrings = new ArrayList<>();
        // implement setOnItemClickListener event on GridView
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                if(isLongClicked){
                    int selectedIndex = customAdapter.selectedPositions.indexOf(position);
                    if (selectedIndex > -1) {
                        customAdapter.selectedPositions.remove(selectedIndex);
                        ((GridItemView) v).display(false);
                        selectedStrings.remove((String) parent.getItemAtPosition(position));
                    } else {
                        customAdapter.selectedPositions.add(position);
                        vv=v;
                        ((GridItemView) v).display(true);
                        selectedStrings.add((String) parent.getItemAtPosition(position));
                    }
                }else{
                    Toast.makeText(DisplayActivity.this,"clicked: "+desc[position],Toast.LENGTH_SHORT).show();
                    //Toast.makeText(DisplayActivity.this,"clicked: "+imageUrl[position],Toast.LENGTH_SHORT).show();
                }
            }
        });

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                int selectedIndex = customAdapter.selectedPositions.indexOf(position);
                vv=v;
                if (selectedIndex > -1) {
                    customAdapter.selectedPositions.remove(selectedIndex);
                    ((GridItemView) v).display(false);
                    selectedStrings.remove((String) parent.getItemAtPosition(position));
                } else {
                    customAdapter.selectedPositions.add(position);
                    ((GridItemView) v).display(true);
                    selectedStrings.add((String) parent.getItemAtPosition(position));

                }
                isLongClicked=true;
                return true;
            }
        });

        //set listener for Button event
        checkSelectedBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isLongClicked) {
                    selectedStrings.clear();
                }
                Intent intent = new Intent(DisplayActivity.this, SelectedItemsActivity.class);
                intent.putStringArrayListExtra("SELECTED_ITEM", selectedStrings);
                startActivity(intent);
            }
        });
    }

}